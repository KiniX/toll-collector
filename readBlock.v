`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:42:44 11/28/2012 
// Design Name: 
// Module Name:    readBlock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module readBlock(RxD, TxD, clk25, dataBlock, pulse, stop);
	input clk25;
	input RxD;
	output TxD;
	input stop;
	output reg pulse;	// when read a valid data
	output reg [31:0] dataBlock;
	
	wire [7:0] RxD_data;
	reg [7:0] TxD_data;
	wire RxD_data_ready;
	reg TxD_start;
	wire TxD_busy;
	reg TxD_enable;
	reg [2:0] state0;	// to read data
	reg [2:0] state1;	// to send reading signal
	reg [23:0] waitCounter;
	
	async_receiver deserializer(.clk(clk25), .RxD(RxD), .RxD_data_ready(RxD_data_ready), .RxD_data(RxD_data));
	async_transmitter serializer(.clk(clk25), .TxD(TxD), .TxD_start(TxD_start), .TxD_data(TxD_data), .TxD_busy(TxD_busy));

	initial begin
		state0 = 0;
		state1 = 0;
	end
	
	// start: send reading signal
	always@(posedge clk25) begin
		if(!TxD_busy)
			TxD_enable = ~TxD_enable;
		if(stop)
			state1 <= 3'h0;
		if(waitCounter != 24'h000000) begin
			waitCounter <= waitCounter-1;
		end
		if(state0 == 3'b000) begin
			TxD_start <= 0;
			if(TxD_enable) begin
				if(state1 == 3'h0) begin
					state1 <= 3'h1;
				end				
				if(state1 == 3'h1) begin
					TxD_start <= 1;
					TxD_data <= 8'h05;
					state1 <= 3'h2;
				end
				if(state1 == 3'h2) begin
					TxD_start <= 1;
					TxD_data <= 8'h02;
					state1 <= 3'h3;
				end
				if(state1 == 3'h3) begin
					TxD_start <= 1;
					TxD_data <= 8'h20;
					state1 <= 3'h4;
				end
				if(state1 == 3'h4) begin
					TxD_start <= 1;
					TxD_data <= 8'h00;
					state1 <= 3'h5;
				end
				if(state1 == 3'h5) begin
					TxD_start <= 1;
					TxD_data <= 8'hFF;
					state1 <= 3'h6;
					waitCounter <= 24'hFFFFFF;
				end
				if(state1==3'h6 && waitCounter==24'h000000) begin
					state1 <= 3'h0;
				end
			end			
		end
	end
	// end: send reading signal
	
	// start: reading data
	always@(posedge clk25) begin 
		pulse <= 0;
		if(RxD_data_ready) begin
			if(state0 == 3'b000) begin
				if(RxD_data == 8'h07)
					state0 <= 3'b001;
				else
					state0 <= 3'b000;
			end
			if(state0 == 3'b001) begin
				state0 <= 3'b010;
			end
			if(state0 == 3'b010) begin
				dataBlock[31:24] = RxD_data;
				state0 <= 3'b011;
			end
			if(state0 == 3'b011) begin
				dataBlock[23:16] = RxD_data;
				state0 <= 3'b100;
			end
			if(state0 == 3'b100) begin
				dataBlock[15:8] = RxD_data;
				state0 <= 3'b101;
			end
			if(state0 == 3'b101) begin
				dataBlock[7:0] = RxD_data;
				state0 <= 3'b110;
			end	
			if(state0 == 3'b110) begin
				state0 <= 3'b000;
				if(RxD_data == 8'hFF) begin
					pulse <= 1;
				end
			end
		end
	end
	// end: reading data

endmodule

