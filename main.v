`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:05:40 11/27/2012 
// Design Name: 
// Module Name:    main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main(RxD, TxD, clk, led, btn);
	input clk;
	input RxD;
	output TxD;
	input btn;
	output reg [7:0] led;
	
	reg clk25;
	wire startup;
	wire blockReady;
	wire [31:0] data;
	
	always@(posedge clk)
		clk25 <= ~clk25;
		
	rfidConnected test01(.RxD(RxD), .clk25(clk25), .pulse(startup));
	readBlock read01(.RxD(RxD), .TxD(TxD), .clk25(clk25), .dataBlock(data), .pulse(blockReady), .stop(btn));
	
	always@(posedge clk25) begin
		if(blockReady)
			led = ~led;
	end
	

endmodule
