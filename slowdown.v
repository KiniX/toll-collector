`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:06:44 11/28/2012 
// Design Name: 
// Module Name:    slowdown 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module slowdown(clk, clkx);
	input clk;
	output reg clkx;
	
	reg [26:0] count;
	
	always@(posedge clk) begin
		if(count == 25'd25000000) begin
			clkx <= ~clkx;
			count <= 0;
		end
		else
			count <= count+1;
	end

endmodule
