`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:40:14 11/26/2012 
// Design Name: 
// Module Name:    rfidConnected 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rfidConnected(RxD, clk25, pulse);
	input clk25;
	output reg pulse; // startup signal
	input RxD;

	reg [2:0] state;
	wire [7:0] RxD_data;
	wire RxD_data_ready;
	
	initial state = 3'b000;
	
	
	async_receiver deserializer(.clk(clk25), .RxD(RxD), .RxD_data_ready(RxD_data_ready), .RxD_data(RxD_data));
	
	always@(posedge clk25) begin
		pulse <= 0;
		if(RxD_data_ready) begin
			if(state == 3'b000) begin
				if(RxD_data == 8'h05)
					state <= 3'b001;
				else
					state <= 3'b000;
			end
			if(state == 3'b001) begin
				if(RxD_data == 8'h11)
					state <= 3'b010;
				else
					state <= 3'b000;
			end
			if(state == 3'b010) begin
				if(RxD_data == 8'h22)
					state <= 3'b011;
				else
					state <= 3'b000;
			end
			if(state == 3'b011) begin
				if(RxD_data == 8'h33)
					state <= 3'b100;
				else
					state <= 3'b000;
			end
			if(state == 3'b100) begin
				state <= 3'b000;
				if(RxD_data == 8'hFF)
					pulse <= 1;
			end
		end
	end

endmodule
